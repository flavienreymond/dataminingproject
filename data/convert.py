
import json
import codecs

data = json.load(codecs.open("partydata.json",'r', 'utf-8-sig'))
print(data)

columns = data["calendar"]
for el in columns:
    el.replace('<br/>',"/")
    el.replace("<br>","/")

print(columns)
columns.insert(0, "partyName")

rows = data["parties"]
print(rows)

name = "partydata.csv"

with open(name,"w") as f:

    f.write(",".join(columns))
    for row in rows:
        string = row["fullName"]
        for el in row["ratings"]:
            string += "," + str(el)
        f.write("\n"+string)

# README #

This is a R project created for Data Mining course in University of Tartu.

This project covers relations between popularity of Estonian political parties and their financial reports. Our end goal has been creating a good and informative poster about the subject.

In this repository there are two different folders : 'data' for datasets, and 'Rscript' for Rmarkdown scripts, PDF results and visualizations in png files.

There are two types of scripts :

- Three scripts used to clean and explore the data and to try different visualizations for analysing and choosing the best relevant visualizations. These scripts are : expvsinc.rmd, expenses_results.rmd, incomes_results.rmd.

- One script named PosterFile.rmd, which has been used to assemble the best visualizations and from which we have chosen the most relevant ones. These graphs had potential to make it on the final poster, but we have chosen to not include all of them.

The scripts are classic Rmarkdown files, one just has to run them as usually. All dependencies needed in each scripts are described at the beginning of the files.

More information in the project [wiki](https://bitbucket.org/flavienreymond/dataminingproject/wiki/Home).